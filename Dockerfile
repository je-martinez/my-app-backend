FROM node:16.14.0-alpine

# Set the working directory
WORKDIR /usr/app

COPY package*.json ./

RUN npm install --only=production

# Copy the rest of your app's source code from your host to your image filesystem.
COPY . .

RUN addgroup -S nodegroup && \
    adduser -S -D -h /usr/app nodeuser nodegroup && \
    chown -R nodeuser:nodegroup /usr/app

USER nodeuser

# Open the mapped port
EXPOSE 3000

CMD [ "node", "app.js" ]