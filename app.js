const express = require('express')
const app = express();
const port = process.env.PORT || 3000;

//Middleware
app.use(express.json());

//Examples
const router = express.Router();
const router2 = express.Router();

const version = 1;
const message = `<GET> Hello Word v${version}`;

//Mapping Endpoints
router.get('/', (req, res) => {
    const response = {
        success: true,
        message: message,
        statusCode: 200
    };
    res.status(response.statusCode).json(response)
});

router.post('/', (req, res) => {
    const response = {
        success: true,
        message: "<POST> Hello Word",
        statusCode: 200
    };
    res.status(response.statusCode).json(response)
});

router.put('/', (req, res) => {
    const response = {
        success: true,
        message: "<PUT> Hello Word",
        statusCode: 200
    };
    res.status(response.statusCode).json(response)
});

//Mapping Endpoints
router2.get('/', (req, res) => {
    const response = {
        success: true,
        message: message,
        statusCode: 200
    };
    res.status(response.statusCode).json(response)
});

app.use('/', router2);
app.use('/api', router);

app.listen(port, () => console.log(`<API> Listening On Port: ${port}`));


