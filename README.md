## My App Backend

Example of basic API using Express.

## How to use CD / CI

In order to test this project on a ECS Fargate with tasks that run docker images, you need to set the next environment variables:

1. `AWS_ACCESS_KEY_ID`
2. `AWS_SECRET_ACCESS_KEY`
3. `AWS_DEFAULT_REGION`
4. `CLUSTER_NAME`
5. `SERVICE_NAME`

To create an Amazon Access Key and Secret Access Key you can check the next documentation:

https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html

You can see your `CLUSTER_NAME` in the section of **Elastic Container Services** on your Amazon Console. Inside of your Cluster you can access to the **Services Tab** to get the `SERVICE_NAME`, also you see the `AWS_DEFAULT_REGION` in the Cluster Detail.
